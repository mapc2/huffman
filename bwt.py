class BWT:
    def __init__(self, string, number):
        self.array = []
        self.string, self.number = string, number
        self.len_of_string = len(self.string)

    def add(self):
        for i in range(self.len_of_string):
            self.array[i] = self.string[i] + self.array[i]

    def sort(self):
        self.array.sort()

    def run(self):
        self.array = [''] * self.len_of_string

        for i in range(self.len_of_string):  # O(n)
            self.add()  # O(n)
            self.sort()  # O(n log n)

        return self.array[self.number - 1]


def main():
    # s, n = 'ААМЛМРЫАУА__ММ', 9

    s, n = input().split(" ")
    print(BWT(s, int(n)).run())


if __name__ == '__main__':
    main()

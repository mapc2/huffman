def run(code, alphabet):
    result = []
    for number in code:  # O(n)
        result.append(alphabet[number])
        alphabet.insert(0, alphabet.pop(number))  # O(n)
    return result


def main():
    code, alphabet = '4 3 2 4 3 2 0 0 0 4 0', 'а б д к р'
    # code, alphabet = input().split(" ")
    print("".join(run([int(i) for i in code.split()], alphabet.split())))


if __name__ == '__main__':
    main()

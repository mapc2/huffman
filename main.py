import heapq
from collections import namedtuple


class Node(namedtuple("Node", ['left', 'right'])):
    def walk(self, code, acc):
        self.left.walk(code, acc + "0")
        self.right.walk(code, acc + "1")


class Leaf(namedtuple("Leaf", ['char'])):
    def walk(self, code, acc):
        code[self.char] = acc or '0'


def huffman_encode(a, p):
    h = []
    for i in range(len(a)):
        h.append((p[i], len(h), Leaf(a[i])))

    heapq.heapify(h)

    count = len(h)

    while len(h) > 1:  # O(n)
        freq1, _count1, left = heapq.heappop(h)  # O(log n)
        freq2, _count2, right = heapq.heappop(h)  # O(log n)
        heapq.heappush(h, (freq1 + freq2, count, Node(left, right)))  # O(log n)
        count += 1

    code = {}
    if h:
        [(_freq, _count, root)] = h
        root.walk(code, "")
    return code


def main():
    a = input().split(" ")
    p = [float(i) for i in input().split(" ")]

    # a = "a b c d e f g h".split(" ")
    # p = [float(i) for i in '0.05 0.1 0.05 0.2 0.03 0.15 0.01 0.04'.split(" ")]

    assert len(a) == len(p)

    code = huffman_encode(a, p)
    for ch in sorted(code):
        print("{}: {}".format(ch, code[ch]))

    s = input()
    encoded = "".join(code[ch] for ch in s)
    print(encoded)


if __name__ == '__main__':
    main()
